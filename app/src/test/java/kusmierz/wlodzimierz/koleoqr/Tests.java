package kusmierz.wlodzimierz.koleoqr;

import org.junit.Test;

import kusmierz.wlodzimierz.koleoqr.Utils.JsonFixer;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class Tests {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void jsonFixer_isWorking() throws Exception {

        String wrong = "{\"ticket_number\": \"ABCDEF\", \"valid\": \"false\", name: \"John Doe\" }";
        String right = "{\"ticket_number\": \"ABCDEF\", \"valid\": \"false\", \"name\": \"John Doe\" }";

        assertEquals(right, JsonFixer.JsonNameFixer(wrong));
    }

    @Test
    public void jenkins_test() throws Exception {

        Boolean test=true;

        assertEquals(test, true);
    }
}