package kusmierz.wlodzimierz.koleoqr.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;


import kusmierz.wlodzimierz.koleoqr.Interfaces.Scanner;
import kusmierz.wlodzimierz.koleoqr.R;
import kusmierz.wlodzimierz.koleoqr.Utils.JsonFixer;
import kusmierz.wlodzimierz.koleoqr.Utils.QrScanner;
import kusmierz.wlodzimierz.koleoqr.Utils.Ticket;

public class MainActivity extends AppCompatActivity {


    private TextView name;
    private TextView valid;
    private TextView number;
    private Button scanButton;
    private Scanner scanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        name = (TextView) findViewById(R.id.textViewNameValue);
        valid = (TextView) findViewById(R.id.textViewValidValue);
        number = (TextView) findViewById(R.id.textViewNumberValue);
        scanButton = (Button) findViewById(R.id.buttonScan);

        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanner = new QrScanner();
                scanner.runScan(MainActivity.this);
            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() == null) {
                Log.d("MainActivity", "Cancelled scan");
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {

                Gson gson = new Gson();
                String jsonInString = JsonFixer.JsonNameFixer(result.getContents());
                Ticket ticket = gson.fromJson(jsonInString, Ticket.class);

                if(ticket != null)
                {
                    number.setText(ticket.getTicketNumber());
                    name.setText(ticket.getName());

                    if(ticket.getValid().equals("true"))
                    {
                        valid.setText("Valid");
                        valid.setTextColor(Color.parseColor("#69A264"));
                    }
                    else
                    {
                        valid.setText("Not Valid");
                        valid.setTextColor(Color.parseColor("#ce0000"));
                    }

                    switchLayouts();

                }
                else
                {
                    Toast.makeText(this, "Wrong QR code", Toast.LENGTH_LONG).show();
                }


            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    private void switchLayouts()
    {
        LinearLayout layoutNotScanned = (LinearLayout) findViewById(R.id.ticketNotScannedLayout);
        LinearLayout layoutScanned = (LinearLayout) findViewById(R.id.ticketScannedLayout);

        layoutNotScanned.setVisibility(View.GONE);
        layoutScanned.setVisibility(View.VISIBLE);
    }







}
