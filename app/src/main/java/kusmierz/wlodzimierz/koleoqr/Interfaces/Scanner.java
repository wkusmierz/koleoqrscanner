package kusmierz.wlodzimierz.koleoqr.Interfaces;


import android.content.Context;

public interface Scanner {

    void runScan(Context context);
}
