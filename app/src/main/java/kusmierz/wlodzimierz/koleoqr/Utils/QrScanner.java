package kusmierz.wlodzimierz.koleoqr.Utils;

import android.app.Activity;
import android.content.Context;

import com.google.zxing.integration.android.IntentIntegrator;

import kusmierz.wlodzimierz.koleoqr.Interfaces.Scanner;


public class QrScanner implements Scanner{
    @Override
    public void runScan(Context context) {

        IntentIntegrator integrator = new IntentIntegrator((Activity) context);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setPrompt("Scan");
        integrator.setCameraId(0);
        integrator.setBeepEnabled(false);
        integrator.setBarcodeImageEnabled(false);
        integrator.initiateScan();
    }
}
