package kusmierz.wlodzimierz.koleoqr.Utils;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Ticket {

    @SerializedName("ticket_number")
    @Expose
    private String ticketNumber;
    @SerializedName("valid")
    @Expose
    private String valid;
    @SerializedName("name")
    @Expose
    private String name;


    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}